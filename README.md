# Riditech Frontend

Generador aleatorio de nombres de startups con palabras clave.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Deployment
See [General Guidelines](https://cli.vuejs.org/guide/deployment.html#general-guidelines)

### Desplegar a caprover
Ejecutar el comando siguiente para empaquetar los archivos estaticos a subir.

```bash
npm run build
```

```bash
./create-tar-file.sh
```

```bash
caprover deploy -t ./deploy.tar
```

## Screenshot
![](screenshot.png)
