import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Client from '../views/Client.vue'
import Clients from '../views/Clients.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
}, {
    path: '/clients',
    name: 'Clients',
    component: Clients
}, {
    path: '/clients/:id',
    name: 'Client',
    component: Client
}]

const router = new VueRouter({
    routes
})

export default router